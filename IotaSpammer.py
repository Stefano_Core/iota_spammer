import iota
import sys
import os


"Check IOTA node health"
def printHealth(nodeURL, api):
    print("\n\n### NODE HEALTH ##")
    nodeHealth = api.get_node_info()

    node_appVersion = nodeHealth['appVersion']
    node_latestMilestone = nodeHealth['latestMilestone']
    node_latestMilestoneIndex = nodeHealth['latestMilestoneIndex']
    node_latestSolidSubtangleMilestone = nodeHealth['latestSolidSubtangleMilestone']
    node_latestSolidSubtangleMilestoneIndex = nodeHealth['latestSolidSubtangleMilestoneIndex']
    node_neighbors = nodeHealth['neighbors']
    node_tips = nodeHealth['tips']
    node_transactionsToRequest = nodeHealth['transactionsToRequest']

    print('appVersion: ' + str(node_appVersion))
    print('latestMilestone: ' + str(node_latestMilestone))
    print('latestMilestoneIndex: ' + str(node_latestMilestoneIndex))
    print('latestSolidSubtangleMilestone: ' + str(node_latestSolidSubtangleMilestone))
    print('latestSolidSubtangleMilestoneIndex: ' + str(node_latestSolidSubtangleMilestoneIndex))
    print('neighbors: ' + str(node_neighbors))
    print('tips: ' + str(node_tips))
    print('transactionsToRequest: ' + str(node_transactionsToRequest))


def promoteTX(rx_address, iteration):

    ### TX CREATION ##
    print("\n\n     ### TX CREATION ##")
    tx_message = "See you here: https://t.me/IotaFullNodeITA"
    tx_tag = iota.Tag(b'IOTA9FULL9NODE9ITA')
    tx = iota.ProposedTransaction(address = iota.Address(rx_address), 
                                  message = iota.TryteString.from_unicode(tx_message),
                                  tag = tx_tag, 
                                  value = 0)
    print('     Created first transaction: ')
    print("     " + str(vars(tx)))

    ### BUNDLE FINALIZATION ###
    print("\n\n     ### BUNDLE FINALIZATION ###")
    bundle = iota.ProposedBundle(transactions = [tx])
    bundle.finalize()
    print("     Bundle is finalized...")
    print("     Generated bundle hash: %s" % (bundle.hash))
    print("     List of all transaction in the Bundle:\n")
    for txn in bundle:
        print("     " + str(vars(txn)))
    bundle_trytes = bundle.as_tryte_strings() # bundle as trytes

    ### TIP SELECTION ###
    print("\n\n     ### TIP SELECTION ###")
    tips = api.get_transactions_to_approve(depth = 3)
    print("     " + str(tips))

    ### POW ###
    print("\n\n     ### POW ###")
    try:
        attached_tx = api.attach_to_tangle(trunk_transaction=tips['trunkTransaction'], branch_transaction=tips['branchTransaction'], trytes=bundle_trytes, min_weight_magnitude=14)
    except:
        print('Can\'t execute POW and attach the TX to Tangle')

    ### BROADCASTING ###
    print("     Broadcasting transaction...")
    res = api.broadcast_and_store(attached_tx['trytes'])
    print("     " + str(res))

    ### TRANSACTION RECAP ###
    print("\n\n     ### TRANSACTION RECAP ###")
    print("     " + str(vars(attached_tx['trytes'][0])))

    sent_tx = iota.Transaction.from_tryte_string(attached_tx['trytes'][0])
    print("     Transaction Hash: " + str(sent_tx.hash))

    return ("")

def destroy(iteration):
    print("     Total iterations: " + iteration)
    print('     Exiting script...')

if __name__ == '__main__':     # Program start from here
     rx_address = 'I9LOVE9IOTA9FULL9NODE9ITA99999999999999999999999999999999999999999999999999999999'

     nodeURL = 'https://iota-man1.com:14267'
     api = iota.Iota(nodeURL)
     try:
         printHealth(nodeURL, api)
         iteration = 0
         while True:
             print("ITERATION #" + str(iteration))
             tx_hash_to_be_promoted = promoteTX(rx_address, iteration)
             iteration = iteration + 1

     except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the destroy() will be  executed.
         destroy(iteration)